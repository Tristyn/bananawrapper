﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BananaWrapper.Team
{
    public class TeamStatSummaryDto
    {
        [JsonProperty("fullId")]
        public string FullId { get; set; }

        [JsonProperty("teamStatDetails")]
        public IEnumerable<TeamStatDetailDto> TeamStatDetails { get; set; }
    }
}
