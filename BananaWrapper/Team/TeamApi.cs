﻿using System.Globalization;
using System.Threading.Tasks;

namespace BananaWrapper.Team
{
    class TeamApi : ChildApi
    {
        #region Fields
        protected override Region CompatibleRegions { get { return Region.Brazil | Region.NorthAmerica | Region.Turkey | Region.EuropeNordicEast | Region.EuropeWest; } }
        #endregion

        #region Constructor

        public TeamApi(DevKey devKey)
            : base(devKey)
        {

        }

        #endregion


        #region Methods

        protected override string GetBaseApiAddress(Region region)
        {
            return LolAddress + GetRegionUrl(region) + "/v2.2/team";
        }

        #region Get Team By Summoner

        public TeamDto GetTeamBySummoner(Region region, long summonerId)
        {
            int ignoreStatus;
            return GetRequest<TeamDto>(BuildTeamBySummonerUrl(region, summonerId), out ignoreStatus);
        }

        public TeamDto GetTeamBySummoner(Region region, long summonerId, out int statusCode)
        {
            return GetRequest<TeamDto>(BuildTeamBySummonerUrl(region, summonerId), out statusCode);
        }

        public async Task<TeamDto> GetTeamBySummonerAsync(Region region, long summonerId)
        {
            return await GetRequestAsync<TeamDto>(BuildTeamBySummonerUrl(region, summonerId));
        }

        string BuildTeamBySummonerUrl(Region region, long summonerId)
        {
            var qb = GetQueryBuilderAndVerifyRegion(region);
            qb.AppendAddress("by-summoner");
            qb.AppendAddress(summonerId.ToString(CultureInfo.InvariantCulture));
            return qb.Build();
        }

        #endregion

        #endregion

    }
}
