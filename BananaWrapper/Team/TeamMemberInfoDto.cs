﻿using Newtonsoft.Json;

namespace BananaWrapper.Team
{
    public class TeamMemberInfoDto
    {
        /// <summary>
        /// This json property is really a 'Date' type, but json doesn't 
        /// have a standardized way of representing a date as a string.
        /// </summary>
        [JsonProperty("inviteDate")]
        public string InviteDate { get; set; }

        /// <summary>
        /// This json property is really a 'Date' type, but json doesn't 
        /// have a standardized way of representing a date as a string.
        /// </summary>
        [JsonProperty("joinDate")]
        public string JoinDate { get; set; }

        [JsonProperty("playerId")]
        public long PlayerId { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
    }
}
