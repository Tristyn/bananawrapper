﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BananaWrapper.Team
{
    public class RosterDto
    {
        [JsonProperty("memberList")]
        public IEnumerable<TeamMemberInfoDto> MemberList { get; set; }

        [JsonProperty("ownerId")]
        public long OwnerId { get; set; }
    }
}
