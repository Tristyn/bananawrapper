﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BananaWrapper.Team
{
    class TeamDto
    {
        /// <summary>
        /// This json property is really a 'Date' type, but json doesn't 
        /// have a standardized way of representing a date as a string.
        /// </summary>
        [JsonProperty("createDate")]
        public string createDate { get; set; }

        [JsonProperty("fullId")]
        public string fullId { get; set; }

        /// <summary>
        /// This json property is really a 'Date' type, but json doesn't 
        /// have a standardized way of representing a date as a string.
        /// </summary>
        [JsonProperty("lastGameDate")]
        public string lastGameDate { get; set; }

        /// <summary>
        /// This json property is really a 'Date' type, but json doesn't 
        /// have a standardized way of representing a date as a string.
        /// </summary>
        [JsonProperty("lastJoinDate")]
        public string lastJoinDate { get; set; }

        /// <summary>
        /// This json property is really a 'Date' type, but json doesn't 
        /// have a standardized way of representing a date as a string.
        /// </summary>
        [JsonProperty("lastJoinedRankedTeamQueueData")]
        public string lastJoinedRankedTeamQueueData { get; set; }

        [JsonProperty("matchHistory")]
        public IEnumerable<MatchHistorySummaryDto> MatchHistory { get; set; }

        [JsonProperty("messageOfDay")]
        public MessageOfDayDto MessageOfDay { get; set; }

        /// <summary>
        /// This json property is really a 'Date' type, but json doesn't 
        /// have a standardized way of representing a date as a string.
        /// </summary>
        [JsonProperty("modifyDate")]
        public string modifyDate { get; set; }

        [JsonProperty("name")]
        public string name { get; set; }

        [JsonProperty("roster")]
        public RosterDto Roster { get; set; }

        /// <summary>
        /// This json property is really a 'Date' type, but json doesn't 
        /// have a standardized way of representing a date as a string.
        /// </summary>
        [JsonProperty("secondLastJoinDate")]
        public string SecondLastJoinDate { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("tag")]
        public string Tag { get; set; }

        [JsonProperty("teamStatSummary")]
        public TeamStatSummaryDto TeamStatSummary { get; set; }

        /// <summary>
        /// This json property is really a 'Date' type, but json doesn't 
        /// have a standardized way of representing a date as a string.
        /// </summary>
        [JsonProperty("thirdLastJoinDate")]
        public string ThirdLastJoinDate { get; set; }
    }
}
