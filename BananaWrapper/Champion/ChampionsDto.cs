﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BananaWrapper.Champion
{
    public class ChampionsDto : LolDto<ChampionsDto>
    {
        #region Fields

        // ReSharper disable InconsistentNaming
        [JsonProperty("champions")]
        public IEnumerable<ChampionDto> champions { get; set; }
        // ReSharper restore InconsistentNaming

        #endregion
    }
}
