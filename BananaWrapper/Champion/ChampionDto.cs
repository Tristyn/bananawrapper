﻿using Newtonsoft.Json;

namespace BananaWrapper.Champion
{
    /// <summary>
    /// ChampionApi stats. Info is found in lolclient>player>champions>champ
    /// </summary>
    public struct ChampionDto
    {
        #region Properties
        /// <summary>
        /// .id that uniquely identifies each champion.
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }
        /// <summary>
        /// Friendly name for the champion.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// Indicates if the champion is active.
        /// </summary>
        [JsonProperty("active")]
        public bool Active { get; set; }
        /// <summary>
        /// ChampionApi attack rank.
        /// </summary>
        [JsonProperty("attackRank")]
        public int AttackRank { get; set; }
        /// <summary>
        /// ChampionApi defense rank.
        /// </summary>
        [JsonProperty("defenseRank")]
        public int DefenseRank { get; set; }
        /// <summary>
        /// ChampionApi magic rank.
        /// </summary>
        [JsonProperty("magicRank")]
        public int MagicRank { get; set; }
        /// <summary>
        /// ChampionApi difficulty rank.
        /// </summary>
        [JsonProperty("difficultyRank")]
        public int DifficultyRank { get; set; }
        /// <summary>
        /// Bot enabled flag (for custom games).
        /// </summary>
        [JsonProperty("botEnabled")]
        public bool BotEnabled { get; set; }
        /// <summary>
        /// Indicates if the champion is free to play. Free to play champions are rotated periodically.
        /// </summary>
        [JsonProperty("freeToPlay")]
        public bool FreeToPlay { get; set; }
        /// <summary>
        /// Bot enabled flag (for custom games).
        /// </summary>
        [JsonProperty("botMmEnabled")]
        public bool BotMmEnabled { get; set; }
        /// <summary>
        /// Ranked play enabled flag.
        /// </summary>
        [JsonProperty("rankedPlayEnabled")]
        public bool RankedPlayEnabled { get; set; }
        #endregion
    }
}
