﻿using System;
using System.Threading.Tasks;

namespace BananaWrapper.Champion
{
    /// <summary>
    /// Provides services for querying the generally static champion information.
    /// </summary>
    public class ChampionApi : ChildApi
    {
        #region Fields
        protected override Region CompatibleRegions { get { return Region.NorthAmerica | Region.EuropeWest | Region.EuropeNordicEast; } }
        #endregion

        #region Constructor

        /// <summary>
        /// Constructs a new champion Api using the Key.
        /// </summary>
        /// <param name="devKey"></param>
        internal ChampionApi(DevKey devKey)
            : base(devKey)
        {

        }

        #endregion

        #region Methods

        protected override string GetBaseApiAddress(Region region)
        {
            return LolAddress + GetRegionUrl(region) + "/v1.1/champion";
        }

        #region GetChampions

        /// <summary>
        /// Gets all champions available in the region.
        /// Only compatible with NorthAmerica, EuropeWest, EuropeNordicEast.
        /// Uses Champions Api v1.1
        /// </summary>
        /// <param name="region">Region where to retrieve the data.</param>
        /// <param name="flags">Optional query flags.</param>
        /// <exception cref="ArgumentException">Region isn't compatible.</exception>
        /// <returns>An enumerable list of all champions. Can be null.</returns>
        public ChampionsDto GetChampions(Region region, ChampionFlags flags)
        {
            int ignoreStatus;
            return GetChampions(region, flags, out ignoreStatus);
        }

        /// <summary>
        /// Gets all champions available in the region.
        /// Only compatible with NorthAmerica, EuropeWest, EuropeNordicEast.
        /// Uses Champions Api v1.1
        /// </summary>
        /// <param name="region">Region where to retrieve the data.</param>
        /// <param name="flags">Optional query flags.</param>
        /// <param name="statusCode">The status code of the html get request.</param>
        /// <exception cref="ArgumentException">Region isn't compatible.</exception>
        /// <exception cref="RateLimitException"></exception>
        /// <returns>An enumerable list of all champions. Can be null.</returns>
        public ChampionsDto GetChampions(Region region, ChampionFlags flags, out int statusCode)
        {
            return GetRequest<ChampionsDto>(BuildChampionUrl(region, flags), out statusCode);
        }

        public async Task<ChampionsDto> GetChampionsAsync(Region region,ChampionFlags flags)
        {
            return await GetRequestAsync<ChampionsDto>(BuildChampionUrl(region, flags));
        }

        private string BuildChampionUrl(Region region, ChampionFlags flags)
        {
            var qb = GetQueryBuilderAndVerifyRegion(region);
            qb.AppendParameterIfTrue("freeToPlay", (flags & ChampionFlags.FreeToPlay) == ChampionFlags.FreeToPlay);
            return qb.Build();
        }

        #endregion

        #endregion
    }

    /// <summary>
    /// Optional query flags used in GetChampions
    /// </summary>
    [Flags]
    public enum ChampionFlags
    {
        // ReSharper disable CSharpWarnings::CS1591
        None = 0,
        FreeToPlay = 1 << 0,
        // ReSharper restore CSharpWarnings::CS1591
    }
}
