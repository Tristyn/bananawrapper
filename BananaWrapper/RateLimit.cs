﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Threading;
using System.Threading.Tasks;

namespace BananaWrapper
{
    public class RateLimit
    {
        #region Fields
        /// <summary>
        /// Your rate limit in seconds.
        /// </summary>
        public int RequestSpan { get; private set; }
        /// <summary>
        /// The maximum amount of requests you can make in the time span.
        /// </summary>
        public int MaxRequests { get; private set; }
        readonly Queue<int> _unixTimeRequests = new Queue<int>();
        #endregion

        #region Constructor

        /// <summary>
        /// Construct a new rate limit with the maximum requests and request span
        /// </summary>
        /// <param name="maxRequests">
        /// The maximum amount of requests that can be made in a span of time. 
        /// A maxRequests of 0 will cause CanRequest() to allways return false.
        /// maxRequests can't be negative.
        /// </param>
        /// <param name="requestSpan">
        /// The span of time in seconds where requests can be made.
        /// Must be greater than 0
        /// </param>
        public RateLimit(int maxRequests, int requestSpan)
        {
            if (maxRequests < 0)
                throw new ArgumentException("maxRequests");

            if (requestSpan <= 0)
                throw new ArgumentException("requestSpan");

            MaxRequests = maxRequests;
            RequestSpan = requestSpan;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Checks if a request can be made under the current rate limit.
        /// Does not keep track of a request this method returns true.
        /// </summary>
        /// <returns>If a request can be made under this rate limit.</returns>
        [Pure]
        public bool CanRequest()
        {
            lock (_unixTimeRequests)
            {
                ShearOldRequests();

                return _unixTimeRequests.Count < MaxRequests;
            }
        }

        /// <summary>
        /// Enters a request at the current time, whether it could virtually or not.
        /// </summary>
        public void Request()
        {
            lock (_unixTimeRequests)
            {
                ShearOldRequests();
                _unixTimeRequests.Enqueue(GetUnixTime());
            }
        }

        /// <summary>
        /// Delays until requesting is safe under this rate limit.
        /// </summary>
        public async Task CanRequestAsync()
        {
            int delay;

            lock (_unixTimeRequests)
            {
                if (CanRequest()) return;

                delay = _unixTimeRequests.Peek() + RequestSpan - GetUnixTime();
            }

            // await the difference between the new request and now
            Console.WriteLine("Delaying {0} seconds", delay);
            await Task.Delay(delay * 1000);
            Console.WriteLine("{0} second delay complete", delay);
        }

        /// <summary>
        /// Removes old requests that have are older than RequestSpan
        /// </summary>
        void ShearOldRequests()
        {
            if (_unixTimeRequests.Count <= 0)
                return;

            var now = GetUnixTime();

            while (_unixTimeRequests.Count > 0)
            {
                var requestTime = _unixTimeRequests.Peek();
                if (requestTime + RequestSpan > now)
                    // oldest request isn't older than the RequestSpan, we can stop
                    break;
                _unixTimeRequests.Dequeue();
            }
        }

        [Pure]
        public static int GetUnixTime()
        {
            var dateTimeNow = DateTime.UtcNow - new DateTime(1970, 1, 1);
            return (int)dateTimeNow.TotalSeconds;
        }

        #endregion
    }
}
