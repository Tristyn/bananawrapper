﻿using System.Globalization;
using System.Threading.Tasks;

namespace BananaWrapper.Game
{
    public class GameApi : ChildApi
    {
        #region Fields

        protected override Region CompatibleRegions { get { return Region.NorthAmerica | Region.EuropeNordicEast | Region.EuropeWest; } }

        #endregion

        #region Constructor

        public GameApi(DevKey devKey)
            : base(devKey)
        {

        }

        #endregion

        #region Methods

        protected override string GetBaseApiAddress(Region region)
        {
            return LolAddress + GetRegionUrl(region) + "/game";
        }

        #region RecentGames

        public RecentGamesDto GetRecentGames(Region region, long summonerId)
        {
            int ignoreStatus;
            return GetRecentGames(region, summonerId, out ignoreStatus);
        }

        public RecentGamesDto GetRecentGames(Region region, long summonerId, out int statusCode)
        {
            return GetRequest<RecentGamesDto>(BuildRecentGamesUrl(region, summonerId), out statusCode);
        }

        public async Task<RecentGamesDto> GetRecentGamesAsync(Region region,long summonerId)
        {
            return await GetRequestAsync<RecentGamesDto>(BuildRecentGamesUrl(region, summonerId));
        }

        string BuildRecentGamesUrl(Region region, long summonerId)
        {
            var qb = GetQueryBuilderAndVerifyRegion(region);
            qb.AppendAddress("by-summoner");
            qb.AppendAddress(summonerId.ToString(CultureInfo.InvariantCulture));
            qb.AppendAddress("recent");
            return qb.Build();
        }

        #endregion

        #endregion
    }
}
