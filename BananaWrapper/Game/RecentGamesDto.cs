﻿using System.Collections.Generic;
using BananaWrapper.Summoner;
using Newtonsoft.Json;

namespace BananaWrapper.Game
{
    public class RecentGamesDto : SummonerCrossQueryDto<RecentGamesDto>
    {
        /// <summary>
        /// List of recent games played (max 10).
        /// </summary>
        [JsonProperty("games")]
        public IEnumerable<GameDto> Games { get; set; }
    }
}
