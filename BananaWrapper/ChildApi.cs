﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Newtonsoft.Json;

namespace BananaWrapper
{
    /// <summary>
    /// All sub Api's (SummonerApi, ChampionApi) inherit from this object.
    /// ChildApi itself is useless to a consumer of BananaWrapper
    /// </summary>
    public abstract class ChildApi
    {
        #region Fields

        [NotNull]
        protected DevKey Key { get; private set; }

        protected abstract Region CompatibleRegions { get; }

        protected const string LolAddress = "https://prod.api.pvp.net/api/lol";
        #endregion

        #region Constructor
        protected ChildApi([NotNull] DevKey devKey)
        {
            if (devKey == null) throw new ArgumentNullException("devKey");

            Key = devKey;
        }

        #endregion

        #region Methods

        [NotNull]
        protected abstract string GetBaseApiAddress(Region region);

        /// <summary>
        /// Gets a preconfigured QueryBuilder set to the specified region.
        /// Throws ArgumentException if region isn't compatible.
        /// </summary>
        /// <param name="region">The region to test.</param>
        /// <returns>A QueryBuilder that has its base address set.</returns>
        [NotNull]
        protected QueryBuilder GetQueryBuilderAndVerifyRegion(Region region)
        {
            if ((region & CompatibleRegions) == 0)
                throw new ArgumentException("region must be compatible and not use a combination of bit flags.");

            var qb = new QueryBuilder(GetBaseApiAddress(region), Key.Key);

            return qb;
        }

        /// <summary>
        /// Converts a region enum into the string used to access riots regional servers.
        /// </summary>
        /// <param name="region">The region to convert into a string</param>
        /// <returns>The region string used to access riots regional servers</returns>
        [NotNull]
        protected static string GetRegionUrl(Region region)
        {
            // lookup region uri
            switch (region)
            {
                case Region.NorthAmerica:
                    return "/na";
                case Region.EuropeWest:
                    return "/euw";
                case Region.EuropeNordicEast:
                    return "/eune";
                case Region.Oceania:
                    return "/oce";
                case Region.Brazil:
                    return "/br";
                case Region.Turkey:
                    return "/tr";
                case Region.LatinAmericaNorth:
                    return "/lan";
                case Region.LatinAmericaSouth:
                    return "/las";
                default:
                    throw new ArgumentException("region has a combination of bit flags");
            }
        }

        /// <summary>
        /// Creates an instance of object T using the web response of the uri.
        /// </summary>
        /// <typeparam name="T">The type of object to create an instance of using the get request.</typeparam>
        /// <param name="uri">The web address used to create a get request.</param>
        /// <param name="statusCode">
        /// The status code of the response, 
        /// riot customizes their responses to give extra information about the query.</param>
        /// <exception cref="RateLimitException"></exception>
        /// <returns> An instance of T, may be null if the web request was unsuccessful. </returns>
        [CanBeNull]
        protected T GetRequest<T>(string uri, out int statusCode) where T : class
        {
            if (!Key.TryRequest())
                throw new RateLimitException("A rate limit has been reached.");

            return Deserialize<T>(uri, out statusCode);
        }

        /// <summary>
        /// Creates an instance of object T using the web response of the uri.
        /// Delays the task until rate limits are OK before requesting.
        /// </summary>
        /// <typeparam name="T">The type of object to create an instance of using the get request.</typeparam>
        /// <param name="uri">The web address used to create a get request.</param>
        /// <returns>An instance of T, may be null if the web request was unsuccessful.</returns>
        protected async Task<T> GetRequestAsync<T>(string uri) where T : class
        {
            await Key.RequestAsync();
            int ignoreStatus;
            return Deserialize<T>(uri, out ignoreStatus);
        }

        [CanBeNull]
        private static T Deserialize<T>(string uri, out int statusCode) where T : class
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(uri);
                var response = (HttpWebResponse)request.GetResponse();
                statusCode = (int)response.StatusCode;

                using (var s = response.GetResponseStream())
                {
                    if (s == null)
                    {
                        // unknown error, just freestyle it yo.
                        return null;
                    }

                    using (var sr = new StreamReader(s))
                    using (var reader = new JsonTextReader(sr))
                    {
                        var serializer = new JsonSerializer();

                        return serializer.Deserialize<T>(reader);
                    }
                }
            }
            catch (WebException ex)
            {
                statusCode = (int)((HttpWebResponse)ex.Response).StatusCode;
                return null;
            }
            catch (UriFormatException)
            {
                statusCode = 404;
                return null;
            }
        }

        #endregion
    }

    /// <summary>
    /// The server regions for LoL
    /// </summary>
    [Flags]
    public enum Region
    {
        // ReSharper disable CSharpWarnings::CS1591
        NorthAmerica = 1 << 0,
        EuropeWest = 1 << 1,
        EuropeNordicEast = 1 << 2,
        Brazil = 1 << 3,
        Turkey = 1 << 4,
        Russia = 1 << 5,
        LatinAmericaNorth = 1 << 6,
        LatinAmericaSouth = 1 << 7,
        Oceania = 1 << 8
        // ReSharper restore CSharpWarnings::CS1591
    }

    [Flags]
    public enum Season
    {
// ReSharper disable InconsistentNaming
        SEASON3 = 1 << 0,
        SEASON4 = 1 << 1
// ReSharper restore InconsistentNaming
    }
}
