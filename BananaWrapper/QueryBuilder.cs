﻿using System;
using System.Text;
using JetBrains.Annotations;

namespace BananaWrapper
{
    public class QueryBuilder
    {
        #region Fields
        [NotNull]
        readonly StringBuilder _sb = new StringBuilder();
        [NotNull]
        readonly string _key;
        bool _buildAddressMode = true;
        #endregion

        #region Constructor

        public QueryBuilder([NotNull] string uri, [NotNull] string key)
        {
            if (uri == null) throw new ArgumentNullException("uri");
            if (key == null) throw new ArgumentNullException("key");

            _key = key;
            _sb.Append(uri);
        }

        #endregion

        #region Methods

        public void AppendAddress([CanBeNull] string address)
        {
            if (address == null) throw new ArgumentNullException("address");
            if (!_buildAddressMode) throw new InvalidOperationException();

            _sb.Append('/');
            _sb.Append(address);
        }

        public void AppendParameter([NotNull] string name, [NotNull] string value)
        {
            if (name == null) throw new ArgumentNullException("name");
            if (value == null) throw new ArgumentNullException("value");

            AppendParameterChar();

            _sb.Append(name);
            _sb.Append("=");
            _sb.Append(value);
        }

        public void AppendParameterIfTrue([NotNull] string param, bool isTrue)
        {
            if (param == null) throw new ArgumentNullException("param");
            if (!isTrue) return;

            AppendParameter(param, "true");
        }

        private void AppendKey()
        {
            AppendParameter("api_key", _key);
        }

        private void AppendParameterChar()
        {
            if (_buildAddressMode)
            {
                _sb.Append("?");
                _buildAddressMode = false;
            }
            else
            {
                _sb.Append("&");
            }
        }

        /// <summary>
        /// Appends the api key, then returns the full query address.
        /// </summary>
        /// <returns></returns>
        public string Build()
        {
            AppendKey();
            return _sb.ToString();
        }

        #endregion
    }
}
