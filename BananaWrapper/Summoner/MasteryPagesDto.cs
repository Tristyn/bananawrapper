﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BananaWrapper.Summoner
{
    public class MasteryPagesDto : LolDto<MasteryPagesDto>
    {
        /// <summary>
        /// List of mastery pages associated with the summoner. 
        /// </summary>
        [JsonProperty("pages")]
        public IEnumerable<MasteryPageDto> Pages { get; set; }
    }
}
