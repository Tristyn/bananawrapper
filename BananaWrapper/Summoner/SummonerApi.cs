﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace BananaWrapper.Summoner
{
    public class SummonerApi : ChildApi
    {
        #region Fields
        const string ByNameAddress = "by-name";
        const string MasteriesAddress = "masteries";
        const string RuneAddress = "runes";
        const string NameAddress = "name";

        protected override Region CompatibleRegions { get { return Region.NorthAmerica | Region.EuropeWest | Region.EuropeNordicEast; } }
        #endregion

        #region Constructor

        public SummonerApi(DevKey devKey)
            : base(devKey)
        {

        }

        #endregion

        #region Methods

        protected override string GetBaseApiAddress(Region region)
        {
            return LolAddress + GetRegionUrl(region) + "/v1.4/summoner";
        }

        #region Masteries

        /// <summary>
        /// Gets mastery pages associated with the summoner Id
        /// </summary>
        /// <param name="summonerId">ID of the summoner for which to retrieve mastery pages.</param>
        /// <param name="region">Region where to retrieve the data.</param>
        /// <returns>Mastery page information. Can be null.</returns>
        public MasteryPagesDto GetMasteries(long summonerId, Region region)
        {
            int ignoreStatus;
            return GetMasteries(summonerId, region, out ignoreStatus);
        }

        /// <summary>
        /// Gets mastery pages associated with the summoner Id
        /// </summary>
        /// <param name="summonerId">ID of the summoner for which to retrieve mastery pages.</param>
        /// <param name="region">Region where to retrieve the data.</param>
        /// <param name="statusCode">The status code of the web request.</param>
        /// <returns>Mastery page information. Can be null.</returns>
        public MasteryPagesDto GetMasteries(long summonerId, Region region, out int statusCode)
        {
            return GetRequest<MasteryPagesDto>(BuildMasteriesUrl(summonerId, region), out statusCode);
        }

        /// <summary>
        /// Gets mastery pages associated with the summoner Id
        /// </summary>
        /// <param name="summonerId">ID of the summoner for which to retrieve mastery pages.</param>
        /// <param name="region">Region where to retrieve the data.</param>
        /// <returns>Mastery page information. Can be null.</returns>
        public async Task<MasteryPagesDto> GetMasteriesAsync(long summonerId, Region region)
        {
            return await GetRequestAsync<MasteryPagesDto>(BuildMasteriesUrl(summonerId, region));
        }

        private string BuildMasteriesUrl(long summonerId, Region region)
        {
            var qb = GetQueryBuilderAndVerifyRegion(region);
            qb.AppendAddress(summonerId.ToString(CultureInfo.InvariantCulture));
            qb.AppendAddress(MasteriesAddress);
            return qb.Build();
        }

        #endregion

        #region Runes

        /// <summary>
        /// Gets rune pages associated with the summoner Id
        /// </summary>
        /// <param name="summonerId">ID of the summoner for which to retrieve rune pages.</param>
        /// <param name="region">Region where to retrieve the data.</param>
        /// <returns>Mastery page information. Can be null.</returns>
        public RunePagesDto GetRunePages(long summonerId, Region region)
        {
            int ignoreStatus;
            return GetRunePages(summonerId, region, out ignoreStatus);
        }

        /// <summary>
        /// Gets rune pages associated with the summoner Id
        /// </summary>
        /// <param name="summonerId">ID of the summoner for which to retrieve rune pages.</param>
        /// <param name="region">Region where to retrieve the data.</param>
        /// <param name="statusCode">Status code of the web request.</param>
        /// <returns>Mastery page information. Can be null.</returns>
        public RunePagesDto GetRunePages(long summonerId, Region region, out int statusCode)
        {
            return GetRequest<RunePagesDto>(BuildRunePagesUrl(summonerId, region), out statusCode);
        }

        /// <summary>
        /// Gets rune pages associated with the summoner Id
        /// </summary>
        /// <param name="summonerId">ID of the summoner for which to retrieve rune pages.</param>
        /// <param name="region">Region where to retrieve the data.</param>
        /// <returns>Mastery page information. Can be null.</returns>
        public async Task<RunePagesDto> GetRunePagesAsync(long summonerId, Region region)
        {
            return await GetRequestAsync<RunePagesDto>(BuildRunePagesUrl(summonerId, region));
        }

        private string BuildRunePagesUrl(long summonerId, Region region)
        {
            var qb = GetQueryBuilderAndVerifyRegion(region);
            qb.AppendAddress(summonerId.ToString(CultureInfo.InvariantCulture));
            qb.AppendAddress(RuneAddress);
            return qb.Build();
        }

        #endregion

        #region Summoner By Name

        /// <summary>
        /// Gets information about a summoner, the most useful of which is its Id, which is used for other queries.
        /// Only compatible with NorthAmerica, EuropeWest, EuropeNordicEast.
        /// Uses SummonerApi Api v1.2
        /// </summary>
        /// <param name="name">The name of the Summoner.</param>
        /// <param name="region">Region where to retrieve the data.</param>
        /// <exception cref="ArgumentException">Region isn't compatible.</exception>
        /// <returns>Basic summoner information Can be null.</returns>
        public SummonerDto GetSummonerByName([NotNull] string name, Region region)
        {
            int ignoreStatus;
            return GetSummonerByName(name, region, out ignoreStatus);
        }

        /// <summary>
        /// Gets information about a summoner, the most useful of which is its Id, which is used for other queries.
        /// Only compatible with NorthAmerica, EuropeWest, EuropeNordicEast.
        /// Uses SummonerApi Api v1.2
        /// </summary>
        /// <param name="name">The name of the Summoner.</param>
        /// <param name="region">Region where to retrieve the data.</param>
        /// <param name="statusCode"> </param>
        /// <exception cref="ArgumentException">Region isn't compatible.</exception>
        /// <returns>Basic summoner information Can be null.</returns>
        public SummonerDto GetSummonerByName([NotNull] string name, Region region, out int statusCode)
        {
            return GetRequest<SummonerDto>(BuildSummonerByNameUrl(name, region), out statusCode);
        }

        /// <summary>
        /// Gets information about a summoner, the most useful of which is its Id, which is used for other queries.
        /// Only compatible with NorthAmerica, EuropeWest, EuropeNordicEast.
        /// Uses SummonerApi Api v1.2
        /// </summary>
        /// <param name="name">The name of the Summoner. Not null.</param>
        /// <param name="region">Region where to retrieve the data.</param>
        /// <exception cref="ArgumentException">Region isn't compatible.</exception>
        /// <returns>Basic summoner information Can be null.</returns>
        public async Task<SummonerDto> GetSummonerByNameAsync([NotNull] string name, Region region)
        {
            return await GetRequestAsync<SummonerDto>(BuildSummonerByNameUrl(name, region));
        }

        private string BuildSummonerByNameUrl(string name, Region region)
        {
            if (name == null) throw new ArgumentNullException("name");
            var qb = GetQueryBuilderAndVerifyRegion(region);
            qb.AppendAddress(ByNameAddress);
            qb.AppendAddress(name);
            return qb.Build();
        }

        #endregion

        #region Summoner By Id

        /// <summary>
        /// Gets information about a summoner, the most useful of which is its Id, which is used for other queries.
        /// Only compatible with NorthAmerica, EuropeWest, EuropeNordicEast.
        /// Uses SummonerApi Api v1.2
        /// </summary>
        /// <param name="summonerId">The id of the Summoner.</param>
        /// <param name="region">Region where to retrieve the data.</param>
        /// <exception cref="ArgumentException">Region isn't compatible.</exception>
        /// <returns>Basic summoner information Can be null.</returns>
        public SummonerDto GetSummonerById(long summonerId, Region region)
        {
            int ignoreStatus;
            return GetSummonerById(summonerId, region, out ignoreStatus);
        }

        /// <summary>
        /// Gets information about a summoner, the most useful of which is its Id, which is used for other queries.
        /// Only compatible with NorthAmerica, EuropeWest, EuropeNordicEast.
        /// Uses SummonerApi Api v1.2
        /// </summary>
        /// <param name="summonerId">The id of the Summoner.</param>
        /// <param name="region">Region where to retrieve the data.</param>
        /// <param name="statusCode">The status code of the web request.</param>
        /// <exception cref="ArgumentException">Region isn't compatible.</exception>
        /// <returns>Basic summoner information Can be null.</returns>
        public SummonerDto GetSummonerById(long summonerId, Region region, out int statusCode)
        {
            return GetRequest<SummonerDto>(BuildSummonerByIdUrl(summonerId, region), out statusCode);
        }

        /// <summary>
        /// Gets information about a summoner, the most useful of which is its Id, which is used for other queries.
        /// Only compatible with NorthAmerica, EuropeWest, EuropeNordicEast.
        /// Uses SummonerApi Api v1.2
        /// </summary>
        /// <param name="summonerId">The id of the Summoner.</param>
        /// <param name="region">Region where to retrieve the data.</param>
        /// <exception cref="ArgumentException">Region isn't compatible.</exception>
        /// <returns>Basic summoner information Can be null.</returns>
        public async Task<SummonerDto> GetSummonerByIdAsync(long summonerId, Region region)
        {
            return await GetRequestAsync<SummonerDto>(BuildSummonerByIdUrl(summonerId, region));
        }

        private string BuildSummonerByIdUrl(long summonerId, Region region)
        {
            var qb = GetQueryBuilderAndVerifyRegion(region);
            qb.AppendAddress(summonerId.ToString(CultureInfo.InvariantCulture));
            return qb.Build();
        }

        #endregion

        #region Summoner By Ids

        /// <summary>
        /// Gets the name and id of multiple summoners.
        /// Only compatible with NorthAmerica, EuropeWest, EuropeNordicEast.
        /// Uses SummonerApi Api v1.2
        /// </summary>
        /// <param name="summonerIds">The ids of the summoners.</param>
        /// <param name="region">Region where to retrieve the data.</param>
        /// <exception cref="ArgumentException">Region isn't compatible.</exception>
        /// <returns>Names and ids of multiple summoners. Can be null.</returns>
        public SummonerNameListDto SummonerByIds([NotNull] IEnumerable<long> summonerIds, Region region)
        {
            int ignoreStatus;
            return SummonerByIds(summonerIds, region, out ignoreStatus);
        }

        /// <summary>
        /// Gets the name and id of multiple summoners.
        /// Only compatible with NorthAmerica, EuropeWest, EuropeNordicEast.
        /// Uses SummonerApi Api v1.2
        /// </summary>
        /// <param name="summonerIds">The ids of the summoners.</param>
        /// <param name="region">Region where to retrieve the data.</param>
        /// <param name="statusCode">The status code of the web request.</param>
        /// <exception cref="ArgumentException">Region isn't compatible.</exception>
        /// <returns>Names and ids of multiple summoners. Can be null.</returns>
        public SummonerNameListDto SummonerByIds([NotNull] IEnumerable<long> summonerIds, Region region, out int statusCode)
        {
            return GetRequest<SummonerNameListDto>(BuildSummonerByIdsUrl(summonerIds, region), out statusCode);
        }

        /// <summary>
        /// Gets the name and id of multiple summoners.
        /// Only compatible with NorthAmerica, EuropeWest, EuropeNordicEast.
        /// Uses SummonerApi Api v1.2
        /// </summary>
        /// <param name="summonerIds">The ids of the summoners.</param>
        /// <param name="region">Region where to retrieve the data.</param>
        /// <exception cref="ArgumentException">Region isn't compatible.</exception>
        /// <returns>Names and ids of multiple summoners. Can be null.</returns>
        public async Task<SummonerNameListDto> SummonerByIdsAsync([NotNull] IEnumerable<long> summonerIds, Region region)
        {
            return await GetRequestAsync<SummonerNameListDto>(BuildSummonerByIdsUrl(summonerIds, region));
        }

        private string BuildSummonerByIdsUrl([NotNull] IEnumerable<long> summonerIds , Region region)
        {
            if (summonerIds == null) throw new ArgumentNullException("summonerIds");

            // format ids
            var sb = new StringBuilder();
            sb.Append('/');
            foreach (var id in summonerIds)
            {
                sb.Append(id);
                sb.Append(',');
            }
            sb.Remove(sb.Length - 1, 1);
            var formattedIds = sb.ToString();

            var qb = GetQueryBuilderAndVerifyRegion(region);
            qb.AppendAddress(formattedIds);
            qb.AppendAddress(NameAddress);

            return qb.Build();
        }

        #endregion

        #endregion
    }
}
