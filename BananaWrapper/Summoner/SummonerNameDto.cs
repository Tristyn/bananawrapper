﻿using Newtonsoft.Json;

namespace BananaWrapper.Summoner
{
    public class SummonerNameDto : SummonerCrossQueryDto<SummonerNameDto>
    {
        /// <summary>
        /// Summoner name
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
