﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BananaWrapper.Summoner
{
    public class RunePagesDto : SummonerCrossQueryDto<RunePagesDto>
    {
        /// <summary>
        /// Set of rune pages associated with the summoner.
        /// </summary>
        [JsonProperty("pages")]
        public IEnumerable<RunePageDto> Pages { get; set; }
    }
}
