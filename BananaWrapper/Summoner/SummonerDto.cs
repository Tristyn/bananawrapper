﻿using Newtonsoft.Json;

namespace BananaWrapper.Summoner
{
    /// <summary>
    /// Publicly available information about a summoners profile,
    /// the most important of which is the id, which is used by the rest of the Api.
    /// </summary>
    public class SummonerDto : SummonerCrossQueryDto<SummonerDto>
    {
        /// <summary>
        /// SummonerApi name.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// ID of the summoner icon associated with the summoner.
        /// </summary>
        [JsonProperty("profileIconId")]
        public int ProfileIconId { get; set; }

        /// <summary>
        /// Date summoner was last modified specified as epoch milliseconds.
        /// </summary>
        [JsonProperty("revisionDate")]
        public long RevisionDate { get; set; }

        /// <summary>
        /// SummonerApi level associated with the summoner.
        /// </summary>
        [JsonProperty("summonerLevel")]
        public long SummonerLevel { get; set; }
    }
}
