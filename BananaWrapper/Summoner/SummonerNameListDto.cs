﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BananaWrapper.Summoner
{
    public class SummonerNameListDto : SummonerCrossQueryDto<SummonerNameListDto>
    {
        /// <summary>
        /// The list of summoner name information
        /// </summary>
        [JsonProperty("summoners")]
        public IEnumerable<SummonerNameDto> Summoners { get; set; }
    }
}
