﻿using Newtonsoft.Json;

namespace BananaWrapper.Summoner
{
    public abstract class SummonerCrossQueryDto<T> : LolDto<T> where T : SummonerCrossQueryDto<T>
    {
        #region Fields

        /// <summary>
        /// SummonerApi ID. This is used by the class to cross query other summoner Dtos. 
        /// </summary>
        [JsonProperty("id")]
        public long Id { get; set; }

        #endregion

        #region Methods

        

        #endregion
    }
}
