﻿using System.Globalization;
using System.Threading.Tasks;

namespace BananaWrapper.Stats
{
    public class StatsApi : ChildApi
    {
        #region Fields
        protected override Region CompatibleRegions { get { return Region.EuropeNordicEast | Region.NorthAmerica | Region.EuropeWest; } }
        #endregion

        #region Constructor

        public StatsApi(DevKey devKey)
            : base(devKey)
        {

        }

        #endregion

        #region Methods

        protected override string GetBaseApiAddress(Region region)
        {
            return LolAddress + GetQueryBuilderAndVerifyRegion(region) + "/v1.2/stats";
        }

        #region Get Summary

        /// <summary>
        /// Get player stats summaries by summoner ID. One summary is returned per queue type.
        /// </summary>
        public PlayerStatsSummaryListDto GetSummary(Region region, long summonerId, Season season)
        {
            int ignoreStatus;
            return GetRequest<PlayerStatsSummaryListDto>(BuildSummaryUrl(region, summonerId, season), out ignoreStatus);
        }

        /// <summary>
        /// Get player stats summaries by summoner ID. One summary is returned per queue type.
        /// </summary>
        public PlayerStatsSummaryListDto GetSummary(Region region, long summonerId, Season season, out int statusCode)
        {
            return GetRequest<PlayerStatsSummaryListDto>(BuildSummaryUrl(region, summonerId, season), out statusCode);
        }

        /// <summary>
        /// Get player stats summaries by summoner ID. One summary is returned per queue type.
        /// </summary>
        public async Task<PlayerStatsSummaryListDto> GetSummaryAsync(Region region, long summonerId, Season season)
        {
            return await GetRequestAsync<PlayerStatsSummaryListDto>(BuildSummaryUrl(region, summonerId, season));
        }

        string BuildSummaryUrl(Region region, long summonerId, Season season)
        {
            var qb = GetQueryBuilderAndVerifyRegion(region);
            qb.AppendAddress("by-summoner");
            qb.AppendAddress(summonerId.ToString(CultureInfo.InvariantCulture));
            qb.AppendAddress("summary");
            qb.AppendParameter("season", season.ToString());
            return qb.Build();
        }

        #endregion

        #region Get Ranked

        /// <summary>
        /// Get player stats summaries by summoner ID. One summary is returned per queue type.
        /// </summary>
        public RankedStatsDto GetRankedStats(Region region, long summonerId, Season season)
        {
            int ignoreStatus;
            return GetRequest<RankedStatsDto>(BuildRankedUrl(region, summonerId, season), out ignoreStatus);
        }

        /// <summary>
        /// Get player stats summaries by summoner ID. One summary is returned per queue type.
        /// </summary>
        public RankedStatsDto GetRankedStats(Region region, long summonerId, Season season, out int statusCode)
        {
            return GetRequest<RankedStatsDto>(BuildRankedUrl(region, summonerId, season), out statusCode);
        }

        /// <summary>
        /// Get player stats summaries by summoner ID. One summary is returned per queue type.
        /// </summary>
        public async Task<RankedStatsDto> GetRankedStatsAsync(Region region, long summonerId, Season season)
        {
            return await GetRequestAsync<RankedStatsDto>(BuildRankedUrl(region, summonerId, season));
        }

        string BuildRankedUrl(Region region, long summonerId, Season season)
        {
            var qb = GetQueryBuilderAndVerifyRegion(region);
            qb.AppendAddress("by-summoner");
            qb.AppendAddress(summonerId.ToString(CultureInfo.InvariantCulture));
            qb.AppendAddress("ranked");
            qb.AppendParameter("season", season.ToString());
            return qb.Build();
        }

        #endregion

        #endregion
    }
}
