﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BananaWrapper.Stats
{
    public class RankedStatsDto
    {
        /// <summary>
        /// List of aggregated stats summarized by champion.
        /// </summary>
        [JsonProperty("champions")]
        public IEnumerable<ChampionStatsDto> Champions { get; set; }

        /// <summary>
        /// Date stats were last modified specified as epoch milliseconds.
        /// </summary>
        [JsonProperty("modifyDate")]
        public long ModifyDate { get; set; }

        /// <summary>
        /// Summoner ID.
        /// </summary>
        [JsonProperty("summonerId")]
        public long SummonerId { get; set; }
    }
}
