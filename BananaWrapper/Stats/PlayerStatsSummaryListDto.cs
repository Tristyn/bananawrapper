﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BananaWrapper.Stats
{
    public class PlayerStatsSummaryListDto
    {
        /// <summary>
        /// List of player stats summaries associated with the summoner.
        /// </summary>
        [JsonProperty("playerStatSummaries")]
        public IEnumerable<PlayerStatsSummaryDto> PlayerStatsSummaries { get; set; }

        /// <summary>
        /// Summoner ID.
        /// </summary>
        [JsonProperty("summonerId")]
        public long SummonerId { get; set; }
    }
}
