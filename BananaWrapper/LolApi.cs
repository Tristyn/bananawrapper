﻿using System;
using BananaWrapper.Champion;
using BananaWrapper.Game;
using BananaWrapper.League;
using BananaWrapper.Stats;
using BananaWrapper.Summoner;
using JetBrains.Annotations;

namespace BananaWrapper
{
    /// <summary>
    /// The central object for interfacing with the Riot Games Api.
    /// The Api object will keep track of the rate limits of your Api Key.
    /// Soon, the Api will cache queries.
    /// </summary>
    public class LolApi
    {
        [NotNull]
        public ChampionApi ChampionApi { get; private set; }
        [NotNull]
        public SummonerApi SummonerApi { get; private set; }
        [NotNull]
        public GameApi GameApi { get; private set; }
        [NotNull]
        public LeagueApi LeagueApi { get; private set; }

        [NotNull]
        public StatsApi StatsApi { get; private set; }

        /// <summary>
        /// Constructs the API Container.
        /// </summary>
        /// <param name="devKey">The riot games developer key that is unique to your LoL account. You can get your own at http://developer.riotgames.com/ </param>
        /// <exception cref="ArgumentNullException"></exception>
        public LolApi([NotNull] DevKey devKey)
        {
            if (devKey == null) throw new ArgumentNullException("devKey");

            ChampionApi = new ChampionApi(devKey);
            SummonerApi = new SummonerApi(devKey);
            GameApi = new GameApi(devKey);
            LeagueApi = new LeagueApi(devKey);
            StatsApi = new StatsApi(devKey);
        }
    }
}
