﻿using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;

namespace BananaWrapper.League
{
    public class LeagueApi : ChildApi
    {
        #region Fields
        protected override Region CompatibleRegions { get { return Region.Brazil | Region.Turkey | Region.EuropeNordicEast | Region.NorthAmerica | Region.EuropeWest; } }
        #endregion

        #region Constructor

        public LeagueApi(DevKey devKey)
            : base(devKey)
        {

        }

        #endregion

        #region Methods

        protected override string GetBaseApiAddress(Region region)
        {
            return LolAddress + GetRegionUrl(region) + "/v2.2/league";
        }

        #region Get Leagues

        public Dictionary<string, LeagueDto> GetLeagues(Region region, long summonerId)
        {
            int ignoreStatus;
            return GetLeagues(region, summonerId, out ignoreStatus);
        }

        public Dictionary<string,LeagueDto> GetLeagues(Region region, long summonerId, out int statusCode)
        {
            return GetRequest<Dictionary<string, LeagueDto>>(BuildLeaguesUrl(region, summonerId), out statusCode);
        }

        public async Task<Dictionary<string,LeagueDto>> GetLeaguesAsync(Region region, long summonerId)
        {
            return await GetRequestAsync<Dictionary<string, LeagueDto>>(BuildLeaguesUrl(region, summonerId));
        } 

        string BuildLeaguesUrl(Region region, long summonerId)
        {
            var qb = GetQueryBuilderAndVerifyRegion(region);
            qb.AppendAddress("by-summoner");
            qb.AppendAddress(summonerId.ToString(CultureInfo.InvariantCulture));
            return qb.Build();
        }

        #endregion

        #endregion
    }
}
