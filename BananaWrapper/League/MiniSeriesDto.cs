﻿using Newtonsoft.Json;

namespace BananaWrapper.League
{
    public class MiniSeriesDto
    {
        [JsonProperty("losses")]
        public int losses { get; set; }

        [JsonProperty("progress")]
        public char[] Progress { get; set; }

        [JsonProperty("target")]
        public int Target { get; set; }

        [JsonProperty("timeLeftToPlayMillis")]
        public long TimeLeftToPlayMillis { get; set; }

        [JsonProperty("wins")]
        public int Wins { get; set; }
    }
}
