﻿using System;

namespace BananaWrapper
{
    /// <summary>
    /// provides support for requesting a fresher version of itself.
    /// </summary>
    public abstract class LolDto<T> where T : LolDto<T>
    {
        #region Fields
        public int QueryDate { get; private set; }
        #endregion

        #region Constructor

        protected LolDto()
        {
            QueryDate = RateLimit.GetUnixTime();
        }

        #endregion
    }
}
