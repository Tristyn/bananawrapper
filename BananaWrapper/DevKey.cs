﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace BananaWrapper
{
    public class DevKey
    {
        #region Fields
        [NotNull]
        public string Key { get; private set; }
        [CanBeNull]
        readonly List<RateLimit> _rates;
        [NotNull]
        readonly object _lock = new object();
        #endregion

        #region Constructors

        /// <summary>
        /// Constructs a new Key with no RateLimit's.
        /// RIOT MAY BAN YOUR KEY IF YOU GO OVER THE RATE LIMITS YOU HAVE.
        /// </summary>
        /// <param name="key"></param>
        public DevKey([NotNull] string key)
        {
            if (key == null) throw new ArgumentNullException("key");

            Key = key;
        }

        /// <summary>
        /// Constructs a new Devkey, with a key string, 
        /// and the rateLimits used to abide by riot's TOS.
        /// </summary>
        /// <param name="key">Not null. The Riot Games Dev Key, you can get one at http://developer.riotgames.com/ </param>
        /// <param name="rateLimits">
        /// Rate limits are necessary to not over-request riot's servers.
        /// Rate limits are found at http://developer.riotgames.com/
        /// </param>
        public DevKey([NotNull] string key, params RateLimit[] rateLimits)
            : this(key)
        {
            if (rateLimits == null || !rateLimits.Any())
                return;

            _rates = new List<RateLimit>(rateLimits);
        }

        #endregion

        #region Methods

        public bool CanRequest()
        {
            lock (_lock)
            {
                return _rates == null || _rates.All(rate => rate.CanRequest());
            }
        }

        public void ForceRequest()
        {
            lock (_lock)
            {
                if (_rates == null) return;
                _rates.ForEach(rate => rate.Request());
            }
        }

        /// <summary>
        /// Delays until requesting is safe under the rate limits, then requests
        /// </summary>
        public async Task RequestAsync()
        {
            if (_rates == null) return;

            while (!TryRequest())
            {
                IEnumerable<Task> tasks;
                lock (_lock)
                {
                    tasks = _rates.Select(async rate => await rate.CanRequestAsync());
                }

                await Task.WhenAll(tasks);
            }
        }

        public bool TryRequest()
        {
            lock (_lock)
            {
                if (_rates == null) return true;

                if (_rates.All(rate => rate.CanRequest()))
                {
                    _rates.ForEach(rate => rate.Request());
                    return true;
                }
            }

            return false;
        }

        #endregion
    }
}
