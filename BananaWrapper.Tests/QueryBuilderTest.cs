﻿using System;
using NUnit.Framework;

namespace BananaWrapper.Tests
{
    [TestFixture]
    public class QueryBuilderTest
    {
        [Test]
        public void NullConstructor()
        {
            // ReSharper disable AssignNullToNotNullAttribute
            Assert.Throws<ArgumentNullException>(() => new QueryBuilder(null, null));
            Assert.Throws<ArgumentNullException>(() => new QueryBuilder(null, "asd"));
            Assert.Throws<ArgumentNullException>(() => new QueryBuilder("asd", null));
// ReSharper restore AssignNullToNotNullAttribute
        }

        [Test]
        public void NoParametersWithKey()
        {
            var qb = new QueryBuilder("uri", "keykey");
            var query = qb.Build();
            Assert.True(query == "uri?api_key=keykey");
        }

        [Test]
        public void AppendNull()
        {
            var qb = new QueryBuilder("test string pls ignore","keykey");

            // ReSharper disable AssignNullToNotNullAttribute
            Assert.Throws<ArgumentNullException>(() => qb.AppendParameter(null, "asd"));
            Assert.Throws<ArgumentNullException>(() => qb.AppendParameter("asd", null));
            Assert.Throws<ArgumentNullException>(() => qb.AppendParameter(null, null));
            Assert.DoesNotThrow(() => qb.AppendParameter("asd", "asd"));
            // ReSharper restore AssignNullToNotNullAttribute
        }

        [Test]
        public void CorrectFormat()
        {
            var qb = new QueryBuilder("test string pls ignore", "keykey");
            qb.AppendParameter("green","[0,255,0]");
            Assert.True(qb.Build() == "test string pls ignore?green=[0,255,0]&api_key=keykey");
        }

        [Test]
        public void AppendAddressAfterAppendParameter()
        {
            var qb = new QueryBuilder("test string pls ignore", "keykey");
            Assert.DoesNotThrow(()=>qb.AppendAddress("/CoolAddress"));
            qb.AppendParameter("Soraka","Bananas");
            Assert.Throws<InvalidOperationException>(() => qb.AppendAddress("/nextAdress"));
            Assert.Throws<ArgumentNullException>(() => qb.AppendAddress(null));
            Assert.Throws<ArgumentNullException>(() => qb.AppendParameterIfTrue(null,true));
        }

        [Test]
        public void Append()
        {
            var qb = new QueryBuilder("test string pls ignore", "keykey");

            qb.AppendParameterIfTrue("banana", true);
            var query = qb.Build();
            Assert.True(query == "test string pls ignore?banana=true&api_key=keykey");
        }

        [Test]
        public void CompositeAppend()
        {
            var qb = new QueryBuilder("attentionSummoners.com", "keykey");

            qb.AppendParameter("banana", "wrapped");
            qb.AppendParameter("vayne","stutterStepin");
            qb.AppendParameterIfTrue("ireliaBalanced", false);
            qb.AppendParameterIfTrue("nerfIrelia", true);
            Assert.True(qb.Build() == "attentionSummoners.com?banana=wrapped&vayne=stutterStepin&nerfIrelia=true&api_key=keykey");
        }
    }
}