﻿using System;
using System.Collections.Generic;
using System.Threading;
using NUnit.Framework;

namespace BananaWrapper.Tests
{
    [TestFixture]
    public class DevKeyTest
    {
        [Test]
        public void NullConstructorTest()
        {
            // ReSharper disable ObjectCreationAsStatement
            // ReSharper disable AssignNullToNotNullAttribute

            Assert.Throws<ArgumentNullException>(() => { new DevKey(null, new RateLimit(10,10)); });
            Assert.Throws<ArgumentNullException>(() => { new DevKey(null); });
            Assert.DoesNotThrow(() => { new DevKey("asda", null); });
            Assert.DoesNotThrow(() => { new DevKey("asda"); });

            // ReSharper restore AssignNullToNotNullAttribute
            // ReSharper restore ObjectCreationAsStatement
        }

        [Test]
        public void ForceRequestTest()
        {
            var key = new DevKey("asdas", new RateLimit(2, 100));

            Assert.True(key.CanRequest());
            key.ForceRequest();
            Assert.True(key.CanRequest());
            key.ForceRequest();
            Assert.False(key.CanRequest());
        }

        [Test]
        public void ForceRequestNoRates()
        {
            var key = new DevKey("asdas");

            for (var i = 0; i < 20; i++)
            {
                Assert.True(key.CanRequest());
                key.ForceRequest();
            }
        }

        [Test]
        public void TryRequestTest()
        {
            var key = new DevKey("asdas", new RateLimit(1, 1), new RateLimit(2, 10));

            Assert.True(key.TryRequest());
            // false because 1 second limit
            Assert.False(key.TryRequest());
            Thread.Sleep(1100);
            // true because 1 second limit reset
            Assert.True(key.TryRequest());
            Thread.Sleep(1100);
            // false, because 10 seconds limit
            Assert.False(key.TryRequest());
        }
    }
}