﻿using System;
using NUnit.Framework;

namespace BananaWrapper.Tests
{
    [TestFixture]
    public class LolApiTest
    {
        [Test]
        public void NullConstructorString()
        {
// ReSharper disable UnusedVariable
// ReSharper disable AssignNullToNotNullAttribute
            Assert.Throws<ArgumentNullException>(() => { var container = new LolApi(null); });
// ReSharper restore AssignNullToNotNullAttribute
// ReSharper restore UnusedVariable
        }
    }
}
