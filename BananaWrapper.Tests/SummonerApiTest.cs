﻿using System;
using BananaWrapper.Summoner;
using NUnit.Framework;

namespace BananaWrapper.Tests
{
    [TestFixture]
    public class SummonerApiTest
    {
        [Test]
        public void ConstructorTest()
        {
            Assert.Throws<ArgumentNullException>(() => new SummonerApi(null));
            Assert.DoesNotThrow(() => new SummonerApi(MockChildApi.Key));
        }

        [Test]
        public void GetMasteriesTest()
        {
            var first = MockChildApi.API.SummonerApi.GetMasteries(123, Region.NorthAmerica);
            int secondStatus;
            var second = MockChildApi.API.SummonerApi.GetMasteries(123, Region.NorthAmerica, out secondStatus);
            var third = MockChildApi.API.SummonerApi.GetMasteriesAsync(123, Region.NorthAmerica);
            third.Wait();
            Assert.AreEqual(200, secondStatus);
            Assert.NotNull(first);
            Assert.NotNull(second);
            Assert.NotNull(third.Result);
        }

        [Test]
        public void GetRunesTest()
        {
            var first = MockChildApi.API.SummonerApi.GetRunePages(123, Region.NorthAmerica);
            int secondStatus;
            var second = MockChildApi.API.SummonerApi.GetRunePages(123, Region.NorthAmerica, out secondStatus);
            var third = MockChildApi.API.SummonerApi.GetRunePagesAsync(123, Region.NorthAmerica).Result;
            Assert.AreEqual(200, secondStatus);
            Assert.NotNull(first);
            Assert.NotNull(second);
            Assert.NotNull(third);
        }
    }
}