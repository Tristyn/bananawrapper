﻿using System;
using System.Linq;
using BananaWrapper.Champion;
using BananaWrapper.Summoner;
using NUnit.Framework;

namespace BananaWrapper.Tests
{
    [TestFixture]
    public class ChildApiTest
    {
        readonly MockChildApi _childApi = new MockChildApi();

        [Test]
        public void NullConstructor()
        {
            // ReSharper disable ObjectCreationAsStatement
            Assert.Throws<ArgumentNullException>(() => { new SummonerApi(null); });
            Assert.DoesNotThrow(() => { new SummonerApi(new DevKey("asd")); });
            // ReSharper restore ObjectCreationAsStatement
        }

        [Test]
        public void GetRegionUrlTest()
        {
            var api = new MockChildApi();
            var qb = api.GetQueryBuilderAndVerifyRegionTest(Region.NorthAmerica);
            
            Assert.NotNull(qb);
            var query = qb.Build();
            Assert.True(query == @"baseAddress/na/subAddress?api_key=" + MockChildApi.ApiKey);
        }

        [Test]
        public void Deserialize()
        {

            int statusCode;

            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (MockChildApi.ApiKey == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
#pragma warning disable
                Assert.Inconclusive("the api key in MockChildApi is currently null, give it a live api key and rerun the test.");
#pragma warning restore
            var task = MockChildApi.Key.RequestAsync();
            task.Wait();
            var champs = _childApi.DeserializeTest<ChampionsDto>(
                "http://prod.api.pvp.net/api/lol/na/v1.1/champion?api_key=" + MockChildApi.ApiKey, out statusCode);

            Assert.NotNull(champs);
            Assert.NotNull(champs.champions.First());
        }

        [Test]
        public void DeserializeFakeUrl()
        {
            int statusCode;

            var summoner = _childApi.DeserializeTest<ChampionsDto>(
                "FakeUrl.kosjfgd", out statusCode);

            Assert.Null(summoner);
        }

        [Test]
        public void Regions()
        {
            var summonerApi = MockChildApi.API.ChampionApi;

            // region thats not supported
            Assert.Throws<ArgumentException>(() => summonerApi.GetChampions(Region.LatinAmericaNorth, ChampionFlags.None));

            // combined regions
            Assert.Throws<ArgumentException>(() => summonerApi.GetChampions(Region.Brazil | Region.NorthAmerica, ChampionFlags.None));

            // proper region
            Assert.DoesNotThrow(() => summonerApi.GetChampions(Region.NorthAmerica, ChampionFlags.None));
        }
    }

    class MockChildApi : ChildApi
    {
        public const string ApiKey = null;
        public static readonly new DevKey Key = new DevKey(ApiKey);
        public static readonly LolApi API = new LolApi(Key);

        protected override Region CompatibleRegions { get { return Region.NorthAmerica | Region.EuropeWest | Region.EuropeNordicEast; } }

        public MockChildApi()
            : base(Key)
        {

        }

        public QueryBuilder GetQueryBuilderAndVerifyRegionTest(Region region)
        {
            return GetQueryBuilderAndVerifyRegion(region);
        }

        public T DeserializeTest<T>(string url, out int statusCode) where T : class
        {
            return GetRequest<T>(url, out statusCode);
        }

        protected override string GetBaseApiAddress(Region region)
        {
            return "baseAddress" + GetRegionUrl(region) + "/subAddress";
        }
    }
}