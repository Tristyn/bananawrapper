﻿using System;
using System.Threading;
using NUnit.Framework;

namespace BananaWrapper.Tests
{
    [TestFixture]
    public class RateLimitTest
    {
        [Test]
        public void ConstructorArgumentExceptions()
        {
            // ReSharper disable ObjectCreationAsStatement, Testing constructor exceptions
            Assert.Throws<ArgumentException>(() => { new RateLimit(-1, 600); });
            Assert.Throws<ArgumentException>(() => { new RateLimit(-5, 10); });
            Assert.Throws<ArgumentException>(() => { new RateLimit(20, -100); });
            Assert.Throws<ArgumentException>(() => { new RateLimit(20, 0); });
            Assert.DoesNotThrow(() => { new RateLimit(0, 600); });
            // ReSharper restore ObjectCreationAsStatement
        }

        [Test]
        public void RequestLimit()
        {
            var limit = new RateLimit(10, 20);

            for (var i = 0; i < 10; i++)
            {
                limit.Request();
            }

            Assert.IsFalse(limit.CanRequest());
        }

        [Test]
        public void CanRequestLimit()
        {
            var limit = new RateLimit(1, 100);

            Assert.IsTrue(limit.CanRequest());
            limit.Request();
            Assert.IsFalse(limit.CanRequest());
        }

        [Test]
        public void OldRequestsSheared()
        {
            var limit = new RateLimit(1, 1);

            limit.Request();
            Assert.IsFalse(limit.CanRequest());
            Thread.Sleep(1100);
            Assert.IsTrue(limit.CanRequest());
        }
    }
}
