﻿using BananaWrapper.Champion;
using System.Linq;
using NUnit.Framework;

namespace BananaWrapper.Tests
{
    [TestFixture]
    public class ChampionApiTest
    {
        [Test]
        public void GetChampionsTest()
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (MockChildApi.ApiKey == null) Assert.Inconclusive();
            // ReSharper restore ConditionIsAlwaysTrueOrFalse

            var api = MockChildApi.API.ChampionApi;

            var allChamps = api.GetChampions(Region.NorthAmerica, ChampionFlags.None);
            var ftpChamps = api.GetChampions(Region.NorthAmerica, ChampionFlags.FreeToPlay);

            Assert.NotNull(allChamps);
            Assert.NotNull(ftpChamps);

            Assert.True(allChamps.champions.Count() > ftpChamps.champions.Count());
        }
    }
}