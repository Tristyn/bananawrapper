#BananaWrapper

![](https://i.imgur.com/VRfA0Jj.png)

###A Lightweight C# wrapper for the League of Legends API.

This was created in the first few days of the API and no longer works. Some features have been superseded. 

Feel free to base an API proper on this code.

``` csharp
public void Main()
{
    // initialize the api
    var devKey = new DevKey("5b68a98d-7a24-4094-b919-fe425ab5b0c4", new RateLimit(10, 10));
    var api = new LolApi(devKey);

    // Get a summoner by their name
    var morello = api.SummonerApi.GetSummonerByName("Morello", Region.NorthAmerica);

    MessageBox.Show(String.Format("{0}: Level {1}, ID {2}", morello.Name, morello.SummonerLevel, morello.Id));
}
```

Yes, that is my live api key. No, I don't care.
